package com.example.hellorobospice.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.hellorobospice.services.MyRoboSpiceService;
import com.octo.android.robospice.SpiceManager;

public abstract class BaseActivity extends FragmentActivity {

	private SpiceManager spiceManager = new SpiceManager(
			MyRoboSpiceService.class);

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
	}

	@Override
	protected void onStart() {
		spiceManager.start(this);
		super.onStart();
	}

	protected SpiceManager getSpiceManager() {
		return spiceManager;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
