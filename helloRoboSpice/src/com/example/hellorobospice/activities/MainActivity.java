package com.example.hellorobospice.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hellorobospice.R;
import com.example.hellorobospice.comm.requests.SpiceRequestBitmap;
import com.example.hellorobospice.comm.requests.SpiceRequestMyObject;
import com.example.hellorobospice.dto.MyObject;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class MainActivity extends BaseActivity {
	public final String TAG = this.getClass().getSimpleName();
	private TextView mTvResult;
	private ImageView mIvImageResult;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTvResult = (TextView) findViewById(R.id.tv_result);
		mIvImageResult = (ImageView) findViewById(R.id.iv_result);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		getSpiceManager().execute(new SpiceRequestMyObject(),
				new MyObjectRequestListener());
		String imgUrl = "http://colorvisiontesting.com/plate%20with%205.jpg";
		getSpiceManager().execute(new SpiceRequestBitmap(imgUrl),
				new ImageRequestListener());
	}

	public final class MyObjectRequestListener implements
			RequestListener<MyObject> {
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.e(TAG, "SpiceRequestMyObject: Request failed");
		}

		@Override
		public void onRequestSuccess(final MyObject result) {
			mTvResult.setText("Result: " + result.getText());
		}
	}

	public final class ImageRequestListener implements RequestListener<Bundle> {
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.e(TAG, "ImageRequestListener: Request failed");
		}

		@Override
		public void onRequestSuccess(final Bundle result) {
			System.out.println("ImageUrl: " + result.getString("imgUrl"));
			mIvImageResult.setImageBitmap((Bitmap) result
					.getParcelable("imgBitmap"));
		}
	}

}
