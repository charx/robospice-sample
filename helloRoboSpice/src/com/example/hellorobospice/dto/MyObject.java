package com.example.hellorobospice.dto;

public class MyObject {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
