package com.example.hellorobospice.comm.parsers;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellorobospice.dto.MyObject;

public class ParserMyObject {
	public final static String TAG = ParserMyObject.class.getSimpleName();

	public static MyObject parse(JSONObject jsonResult) throws JSONException {

		JSONObject jsonObject = jsonResult;
		MyObject myObject = new MyObject();
		myObject.setText(jsonObject.getString("ip"));		

		return myObject;
	}
}
