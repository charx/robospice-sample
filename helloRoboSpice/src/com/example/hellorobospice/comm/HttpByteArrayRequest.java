package com.example.hellorobospice.comm;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.octo.android.robospice.persistence.exception.SpiceException;

public class HttpByteArrayRequest {

	public final static String TAG = HttpByteArrayRequest.class.getSimpleName();

	public static Bitmap doBitmapRequest(String completeUrl)
			throws SpiceException {

		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(completeUrl);
		HttpResponse getResponse = null;
		Bitmap bmp;
		try {
			getResponse = client.execute(get);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpEntity responseEntity = getResponse.getEntity();
		BufferedHttpEntity httpEntity = null;
		try {
			httpEntity = new BufferedHttpEntity(responseEntity);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream imageStream = null;
		try {
			imageStream = httpEntity.getContent();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bmp = BitmapFactory.decodeStream(imageStream);
		return bmp;
	}
}