package com.example.hellorobospice.comm.requests;

import org.json.JSONObject;

import com.example.hellorobospice.comm.HttpJsonRequest;
import com.example.hellorobospice.comm.parsers.ParserMyObject;
import com.example.hellorobospice.dto.MyObject;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;

public class SpiceRequestMyObject extends SpiceRequest<MyObject> {

	public final String TAG = this.getClass().getSimpleName();

	public SpiceRequestMyObject() {
		super(MyObject.class);
		
	}

	@Override
	public MyObject loadDataFromNetwork() throws SpiceException {
		JSONObject	 jsonResult;
		MyObject myObject = new MyObject();
		try {
			jsonResult = HttpJsonRequest.doJsonArrayRequest("http://ip.jsontest.com/", null);
			myObject = ParserMyObject.parse(jsonResult);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SpiceException(e.getMessage());
		}

		return myObject;
	}
}