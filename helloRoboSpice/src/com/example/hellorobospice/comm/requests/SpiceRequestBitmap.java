package com.example.hellorobospice.comm.requests;

import android.graphics.Bitmap;
import android.os.Bundle;

import com.example.hellorobospice.comm.HttpByteArrayRequest;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;

public class SpiceRequestBitmap extends SpiceRequest<Bundle> {

	public final String TAG = this.getClass().getSimpleName();
	private String mImgeUrl;

	public SpiceRequestBitmap(String imgUrl) {
		super(Bundle.class);
		mImgeUrl = imgUrl;
	}

	@Override
	public Bundle loadDataFromNetwork() throws SpiceException {

		Bundle imageBundleResponse = new Bundle();
		Bitmap bitmap = HttpByteArrayRequest.doBitmapRequest(mImgeUrl);
		imageBundleResponse.putString("imgUrl", mImgeUrl);
		imageBundleResponse.putParcelable("imgBitmap", bitmap);

		return imageBundleResponse;
	}
}