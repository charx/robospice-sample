package com.example.hellorobospice.comm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;

public class HttpJsonRequest {

	public final static String TAG = HttpJsonRequest.class.getSimpleName();

	public static JSONObject doJsonArrayRequest(String url,
			Map<String, String> paramsMap) throws SpiceException {

		SpiceException spiceException = new SpiceException("");

		HttpClient client = new DefaultHttpClient();

		String paramUrl = "";
		List<NameValuePair> params = new LinkedList<NameValuePair>();
		if (paramsMap != null) {
			Iterator<Entry<String, String>> iterator = paramsMap.entrySet()
					.iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, String> entry = (Map.Entry<String, String>) iterator
						.next();
				params.add(new BasicNameValuePair(entry.getKey(), entry
						.getValue()));
			}
			paramUrl = URLEncodedUtils.format(params, "utf-8");
			paramUrl = "?" + paramUrl;
		}

		HttpGet get = new HttpGet(url);
		// get.setHeader("Accept", "application/json");
		// get.setHeader(API_HEADER_CLIENT_TOKEN_KEY,
		// API_HEADER_CLIENT_TOKEN_VALUE);

		HttpResponse response;

		try {

			response = client.execute(get);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line).append("\n");
			}
			JSONTokener tokener = new JSONTokener(builder.toString());
			JSONObject finalResult = new JSONObject(tokener);

			return finalResult;

		} catch (ClientProtocolException e) {
			Log.e(TAG, ".doJsonRequest(): " + e.getLocalizedMessage());
			e.printStackTrace();
			throw spiceException;
		} catch (IOException e) {
			Log.e(TAG, ".doJsonRequest(): " + e.getLocalizedMessage());
			e.printStackTrace();
			throw spiceException;
		} catch (JSONException e) {
			Log.e(TAG,
					".doJsonRequest(): JSONObject or JSONArray"
							+ e.getLocalizedMessage());
			e.printStackTrace();
			throw spiceException;
		}
	}

}